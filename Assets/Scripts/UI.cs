﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Archero
{
    public class UI : MonoBehaviour
    {
        public static UI instance;
        
        public static Action OnStartGame;

        [SerializeField] private GameObject pausePanel = null;
        [SerializeField] private GameObject mainPanel = null;
        [SerializeField] private GameObject gamePanel = null;
        [SerializeField] private Text textCountDown = null;
        [SerializeField] private FloatingJoystick joystick = null;

        public FloatingJoystick Joystick => joystick;
        public Transform HealthBarParent => gamePanel.transform;
        public Text TextCountDown => textCountDown;

        private void Awake()
        {
            instance = this;
        }
        
        public void OnStart()
        {
            OnStartGame?.Invoke();
            mainPanel.SetActive(false);
            OnPause(false);
            gamePanel.SetActive(true);
        }
        
        public void OnPause(bool isPause)
        {
            Time.timeScale = isPause ? 0f : 1f;
            pausePanel.SetActive(isPause);
        }

        public void GameOver()
        {
            mainPanel.SetActive(true);
            gamePanel.SetActive(false);
        }
    }
}
