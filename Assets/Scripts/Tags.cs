﻿namespace Archero
{
    public class Tags
    {
        public const string PLAYER = "player";
        public const string ENEMY = "enemy";
        public const string GROUND = "ground";
        public const string GATE = "gate";
        public const string BULLET = "bullet";
        public const string PLAYER_BULLET = "playerBullet";
    }
}
