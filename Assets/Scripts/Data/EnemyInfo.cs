﻿using System;
using System.Collections.Generic;
using Archero.Model;
using Archero.View;
using UnityEngine;

namespace Archero.Data
{
    [Serializable]
    public class EnemyData
    {
        [SerializeField] private EnemyDef def = null;
        [SerializeField] private EnemyView prefab = null;
        [SerializeField] private BulletInfo bulletInfo = null;

        public EnemyDef Def => def;
        public EnemyView Prefab => prefab;
        public BulletInfo BulletInfo => bulletInfo;
    }
    
    [CreateAssetMenu(fileName = "EnemyInfo", menuName = "Data/EnemyInfo", order = 1)]
    public class EnemyInfo : ScriptableObject
    {
        [SerializeField] private List<EnemyData> enemies = new List<EnemyData>();

        public List<EnemyData> Enemies => enemies;
    }
}
