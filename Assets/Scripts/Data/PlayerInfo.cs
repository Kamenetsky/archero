﻿using System.Collections;
using System.Collections.Generic;
using Archero.Model;
using UnityEngine;

namespace Archero.Data
{
    [CreateAssetMenu(fileName = "PlayerInfo", menuName = "Data/PlayerInfo", order = 1)]
    public class PlayerInfo : ScriptableObject
    {
        [SerializeField] private Vector3 initPosition = Vector3.zero;
        [SerializeField] private CreatureParams parameters = null;

        public Vector3 InitPosition => initPosition;
        public CreatureParams Parameters => parameters;
    }
}
