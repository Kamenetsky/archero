﻿using UnityEngine;

namespace Archero.Data
{
    [CreateAssetMenu(fileName = "CameraInfo", menuName = "Data/CameraInfo", order = 1)]
    public class CameraInfo : ScriptableObject
    {
        [SerializeField] private Vector3 offset = new Vector3(0f, 10f, 0f);
        [SerializeField] private float dumpSpeed = 1f;
        
        public Vector3 Offset => offset;
        public float DumpSpeed => dumpSpeed;
    }
}
