﻿using System;
using System.Collections.Generic;
using Archero.Model;
using UnityEngine;

namespace Archero.Data
{
    public enum CellType
    {
        Empty = 0,
        Wall = 10,
        Water = 20,
        Trap = 30,
    }
    
    [Serializable]
    public class SpawnPoint
    {
        [SerializeField] private Vector2Int coords = Vector2Int.zero;
        [SerializeField] private EnemyKind enemy = EnemyKind.Blob;

        public Vector2Int Coords => coords;
        public EnemyKind Enemy => enemy;
    }
    
    [Serializable]
    public class CellInfo
    {
        [SerializeField] private CellType cellType = CellType.Wall;
        [SerializeField] private Vector2Int coords = Vector2Int.zero;

        public CellType CellType => cellType;
        public Vector2Int Coords => coords;
    }

    [Serializable]
    public class CellTypeInfo
    {
        [SerializeField] private CellType cellType = CellType.Empty;
        [SerializeField] private List<GameObject> prefabs = new List<GameObject>();
        
        public CellType CellType => cellType;
        public List<GameObject> Prefabs => prefabs;
    }
    
    [CreateAssetMenu(fileName = "FieldInfo", menuName = "Data/FieldInfo", order = 1)]
    public class FieldInfo : ScriptableObject
    {
        public static Vector2Int defaultSize = new Vector2Int(11, 30);
        
        [SerializeField] private float cellSize = 1f;
        [SerializeField] private float borderWidth = 0.5f;
        [SerializeField] private Vector2Int size = new Vector2Int(11, 22);
        [SerializeField] private List<CellInfo> cells = new List<CellInfo>();
        [SerializeField] private List<SpawnPoint> spawnPoints = new List<SpawnPoint>();
        [SerializeField] private List<CellTypeInfo> cellTypeInfo = new List<CellTypeInfo>();

        public float CellSize => cellSize;
        public Vector2Int Size => size;
        public List<CellInfo> Cells => cells;
        public List<SpawnPoint> SpawnPoints => spawnPoints;
        public float BorderWidth => borderWidth;

        public List<GameObject> GetPrefabs(CellType cellType)
        {
            return cellTypeInfo.Find(x => x.CellType == cellType).Prefabs;
        }
    }
}
