﻿using System;
using System.Collections.Generic;
using System.Linq;
using Archero.Controller;
using Archero.Data;
using Archero.Model;
using Archero.View;
using UnityEngine;

namespace Archero
{
    public class EnemyManager : MonoBehaviour
    {
        public event Action OnAllEnemiesDie; 
        
        [SerializeField] private EnemyInfo info = null;
        
        private List<EnemyController> enemies = new List<EnemyController>();
        
        private CoinsManager coinManager;

        private void Awake()
        {
            coinManager = GetComponentInChildren<CoinsManager>();
        }
        
        public void Spawn(List<SpawnPointInfo> spawnPoints, Transform target)
        {
            Clear();
            
            foreach (var it in spawnPoints)
            {
                EnemyData data = info.Enemies.Find(x => x.Def.EnemyKind == it.enemyKind);
                SpawnEnemy(data, it.transform.position, target);
            }
            
            coinManager.SetTarget(target);
        }

        public void Clear()
        {
            foreach (var enemy in enemies)
            {
                enemy.Dispose();
            }
            
            enemies.Clear();
            
            coinManager.Clear();
        }

        public List<Transform> GetTargets()
        {
            List<Transform> targets = new List<Transform>();

            foreach (var enemy in enemies)
            {
                if (enemy.Model.State == EnemyState.Dead)
                    continue;
                
                targets.Add(enemy.View.GetTransform());
            }
            
            return targets;
        }

        public void Activate(bool value)
        {
            foreach (var enemy in enemies)
            {
                enemy.View.Activate(value);
            }
        }

        private void SpawnEnemy(EnemyData data, Vector3 pos, Transform target)
        {
            IEnemy model = new Enemy(data.Def);
            model.OnDestroy += OnEnemyDie;
            IEnemyView view = Instantiate(data.Prefab);
            view.GetTransform().SetParent(transform);
            view.GetTransform().position = pos;
            view.Init(data.Def);
            view.SetBulletInfo(data.BulletInfo);
            view.SetTarget(target);
            EnemyController enemy = new EnemyController(model, view);
            enemies.Add(enemy);
        }

        private void OnEnemyDie(IDestroyable enemy)
        {
            var died = enemies.Find(x => x.Model == enemy);
            coinManager.AddCoin(died.View.GetTransform().position);
            enemy.OnDestroy -= OnEnemyDie;
            int leavesCount = enemies.Count(x => x.Model.State != EnemyState.Dead);
            if (leavesCount == 0)
            {
                coinManager.Collect();
                OnAllEnemiesDie();
            }                
        }
    }
}
