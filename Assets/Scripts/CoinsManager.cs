﻿using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

namespace Archero
{
    public class CoinsManager : MonoBehaviour
    {
        [SerializeField] private GameObject coinPrefab = null;
        [SerializeField] private float flyTime = 1f;
        
        private List<GameObject> coins = new List<GameObject>();
        private Transform target;

        public void SetTarget(Transform target)
        {
            this.target = target;
        }
        
        public void AddCoin(Vector3 pos)
        {
            var go = Instantiate(coinPrefab);
            go.transform.position = pos + Vector3.up * 2f;
            Rigidbody body = go.GetComponent<Rigidbody>();
            body.AddForce(Vector3.up * Random.Range(30f, 50f) + Vector3.left * Random.Range(-10f, 10f), ForceMode.Impulse);
            body.AddTorque(Vector3.left * Random.Range(20f, 50f));
            coins.Add(go);
        }

        public void Collect()
        {
            foreach (var coin  in coins)
            {
                Destroy(coin.GetComponent<Rigidbody>());
                Destroy(coin.GetComponent<MeshCollider>());
                coin.transform.DOMove(target.position, flyTime).OnComplete(() => Destroy(coin));
            }
            coins.Clear();
        }

        public void Clear()
        {
            foreach (var coin in coins)
            {
                Destroy(coin);
            }

            coins.Clear();
        }
    }
}
