﻿using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace Archero.Utils
{
    public class DiscreteDistribution
    {
        private readonly List<float> accumulatedWeights = new List<float>();
        private readonly Random rnd;
        private readonly float totalWeight;

        public DiscreteDistribution(IEnumerable<float> weights, Random rnd2)
        {
            foreach (var f in weights)
            {
                totalWeight += f;
                accumulatedWeights.Add(f);
            }

            rnd = rnd2;
        }

        public int GetRandom()
        {
            var f = rnd.NextDouble() * totalWeight;
            for (int i = 0; i < accumulatedWeights.Count; i++)
            {
                f -= accumulatedWeights[i];
                if (f <= 0)
                {
                    return i;
                }
            }
            return 0;
        }

        public List<float> Probabilities()
        {
            return accumulatedWeights.ConvertAll(x => x / totalWeight);
        }

        public bool IsValid()
        {
            return totalWeight > 0;
        }
    }
}
