﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Archero.Utils
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Camera))]
    public class CameraFitter : MonoBehaviour
    {
        [SerializeField] private float sceneWidth = 10;
        [SerializeField] private float horizontalFoV = 90;
        [SerializeField] private bool fitOnUpdate = false;

        Camera cam;

        private void Start()
        {
            cam = GetComponent<Camera>();
        }

        private void LateUpdate()
        {
            if (!fitOnUpdate)
                return;

            Fit(sceneWidth);
        }

        public void Fit(float width)
        {
            bool isOrthographic = cam.orthographic;
            sceneWidth = width;
            
            if (isOrthographic)
            {
                float unitsPerPixel = sceneWidth / Screen.width;
                float desiredHalfHeight = 0.5f * unitsPerPixel * Screen.height;
                cam.orthographicSize = desiredHalfHeight;
            }
            else
            {
                float halfWidth = Mathf.Tan(0.5f * horizontalFoV * Mathf.Deg2Rad);
                float halfHeight = halfWidth * Screen.height / Screen.width;
                float verticalFoV = 2.0f * Mathf.Atan(halfHeight) * Mathf.Rad2Deg;
                cam.fieldOfView = verticalFoV;
            }
        }
    }
}
