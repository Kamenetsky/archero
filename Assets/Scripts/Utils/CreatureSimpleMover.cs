﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Archero.Utils
{
    public class MoverInput
    {
        public Vector3 move;
    }

    public class CreatureSimpleMover : MonoBehaviour
    {
        private const string forwardAnimParamName = "Forward";
        private const string turnAnimParamName = "Turn";
        private const string attackAnimParamName = "IsAttacking";
        private const string speedAnimParamName = "Speed";
        private const string deadAnimParamName = "Dead";

        private static int forwardAnimHash;
        private static int turnAnimHash;
        private static int attackAnimHash;
        private static int speedAnimHash;
        private static int deadAnimHash;
        
        [SerializeField] private Animator animator = null;

        private float moveSpeed = 1;
        private float movingTurnSpeed = 360;
        private float stationaryTurnSpeed = 180;

        void Awake()
        {
            if (animator == null)
                animator = GetComponentInChildren<Animator>();

            forwardAnimHash = Animator.StringToHash(forwardAnimParamName);
            turnAnimHash = Animator.StringToHash(turnAnimParamName);
            attackAnimHash = Animator.StringToHash(attackAnimParamName);
            speedAnimHash = Animator.StringToHash(speedAnimParamName);
            deadAnimHash = Animator.StringToHash(deadAnimParamName);
        }

        public void Setup(float moveSpeed, float movingTurnSpeed = 360, float stationaryTurnSpeed = 180)
        {
            this.moveSpeed = moveSpeed;
            this.movingTurnSpeed = movingTurnSpeed;
            this.stationaryTurnSpeed = stationaryTurnSpeed;
        }

        public void Move(MoverInput input, bool moveByAnimator = true)
        {
            if (input.move.magnitude > 1f)
                input.move.Normalize();
            
            input.move = transform.InverseTransformDirection(input.move);
            float turnAmount = Mathf.Atan2(input.move.x, input.move.z);
            float forwardAmount = input.move.z;

            if (animator)
                animator.applyRootMotion = moveByAnimator;
            
            if (!moveByAnimator)
                UpdateTransformMove(forwardAmount, turnAmount);

            if (moveByAnimator)
                ApplyExtraTurnRotation(forwardAmount, turnAmount);
            
            UpdateAnimatorMove(forwardAmount, turnAmount);
        }

        public void Stop()
        {
            UpdateTransformMove(0f, 0f);
            UpdateAnimatorMove(0f, 0f);
            if (animator)
                animator.Rebind();
        }

        private void ApplyExtraTurnRotation(float forwardAmount, float turnAmount)
        {
            float addTurnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);
            transform.Rotate(0, turnAmount * addTurnSpeed * Time.deltaTime, 0);
        }

        private void UpdateTransformMove(float forwardAmount, float turnAmount)
        {
            transform.Rotate(0, turnAmount * movingTurnSpeed * Time.deltaTime, 0);
            transform.position += transform.forward * forwardAmount * moveSpeed * Time.deltaTime;
        }

        private void UpdateAnimatorMove(float forwardAmount, float turnAmount)
        {
            if (animator == null)
                return;
            
            animator.SetFloat(speedAnimHash, forwardAmount * moveSpeed, 0.01f, Time.deltaTime);
            animator.SetFloat(forwardAnimHash, forwardAmount * moveSpeed, 0.01f, Time.deltaTime);
            animator.SetFloat(turnAnimHash, turnAmount, 0.01f, Time.deltaTime);
        }

        public void ShowAttack()
        {
            if (animator == null)
                return;
            
             animator.SetTrigger(attackAnimHash);
        }
        
        public void ShowDead()
        {
            if (animator == null)
                return;
            
            animator.SetTrigger(deadAnimHash);
        }
    }
}
