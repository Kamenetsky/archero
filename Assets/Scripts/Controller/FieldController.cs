﻿using System.Collections.Generic;
using Archero.Data;
using Archero.Model;
using Archero.View;
using UnityEngine;

namespace Archero.Controller
{
    public class SpawnPointInfo
    {
        public Transform transform;
        public EnemyKind enemyKind = EnemyKind.Skeleton;
    }
    
    public class FieldController
    {
        private readonly Field model;
        private readonly IFieldView view;
        
        private Dictionary<Cell, GameObject> cellMap = new Dictionary<Cell, GameObject>();

        public Field Model => model;
        public IFieldView View => view;

        public List<SpawnPointInfo> GetSpawnPoints()
        {
            List<SpawnPointInfo> result = new List<SpawnPointInfo>();

            foreach (var kvp in cellMap)
            {
                if (kvp.Key.enemySpawn == EnemyKind.None)
                    continue;
                
                result.Add(new SpawnPointInfo(){transform = kvp.Value.transform, enemyKind = kvp.Key.enemySpawn});
            }

            return result;
        }
        
        public FieldController(Field model, IFieldView view)
        {
            this.model = model;
            this.view = view;

            UpdateView();
        }

        public void Clear()
        {
            cellMap.Clear();
            view.Clear();
        }

        public void ShowGate()
        {
            view.ShowGate(model.Height);
        }

        private void UpdateView()
        {
            for (int i = 0; i < model.SizeX; ++i)
            {
                for (int j = 0; j < model.SizeY; ++j)
                {
                    Cell cell = model[i, j];
                    float x = -model.Width * 0.5f + i * model.CellSize + 0.5f * model.CellSize;
                    float y = -model.Height * 0.5f + j * model.CellSize + 0.5f * model.CellSize;
                    Vector3 position = new Vector3(x, 0, y);
                    bool isOdd = (i % 2 == 0 && j % 2 == 1) || (i % 2 == 1 && j % 2 == 0);
                    if (model.isGenerated)
                    {
                        cellMap.Add(cell, view.CreateCell(position, cell.cellType, isOdd));
                    }
                    else
                    {
                        List<GameObject> prefabs = model.Info.GetPrefabs(cell.cellType);
                        var prefab = (cell.cellType == CellType.Empty ? (isOdd ? prefabs[0] : prefabs[1]) : prefabs[0]);
                        cellMap.Add(cell, view.CreateCell(position, prefab));
                    }
                }
            }
            
            view.CreateBorders(model.Width, model.Height, model.Info != null ? model.Info.BorderWidth : 0.5f);
        }
    }
}
