﻿namespace Archero.Controller
{
    public interface ICreatureController
    {
        void DoUpdate(float deltaTime);
        void DoDestroy();
    }
}
