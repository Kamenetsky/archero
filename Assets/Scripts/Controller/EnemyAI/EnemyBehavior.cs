﻿using System;
using Archero.Model;
using Archero.View;

namespace Archero.Controller.EnemyAI
{
    public abstract class EnemyBehavior : IEnemyBehavior
    {
        public event Action<Decision, float> OnMakeDecision;
        
        protected readonly IEnemy enemy;
        protected Decision currentDecision = Decision.SetIdle;
        private float time;
        protected float nextDecisionTime;

        public EnemyBehavior(IEnemy enemy)
        {
            this.enemy = enemy;
            nextDecisionTime = enemy.Def.Params.StaticTime;
        }

        public virtual void DoUpdate(float deltaTime)
        {
            time += deltaTime;
            if (time > nextDecisionTime)
            {
                time = 0f;
                MakeDecision();
            }
        }

        protected virtual void MakeDecision()
        {
            OnMakeDecision?.Invoke(currentDecision, nextDecisionTime);
        }
    }
}
