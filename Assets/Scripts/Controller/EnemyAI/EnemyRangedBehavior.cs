﻿using Archero.Model;
using Archero.View;
using UnityEngine;

namespace Archero.Controller.EnemyAI
{
    public class EnemyRangedBehavior : EnemyBehavior
    {       
        public EnemyRangedBehavior(IEnemy enemy) : base(enemy)
        {           
        }
        
        protected override void MakeDecision()
        {
            currentDecision = Random.value < 0.1f ? Decision.MoveToRandomPoint : Decision.Shoot;
            currentDecision = Random.value < 0.2f ? Decision.MoveToPlayer : Decision.Shoot;
            nextDecisionTime = currentDecision == Decision.Shoot ? enemy.Def.Params.ShootSpeed :
                enemy.Def.Params.MoveDistance / enemy.Def.Params.MoveSpeed;
            enemy.State = currentDecision == Decision.Shoot ? EnemyState.Shooting : EnemyState.Moving;
            
            base.MakeDecision();
        }
    }
}
