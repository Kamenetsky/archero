﻿using System;
using System.Collections.Generic;
using Archero.Model;

namespace Archero.Controller.EnemyAI
{
    public static class EnemyBehaviorFactory
    {
        private static readonly Dictionary<EnemyKind, Type> map = new Dictionary<EnemyKind, Type>
        {
            {EnemyKind.Skeleton, typeof(EnemyRangedBehavior)},
            {EnemyKind.Blob, typeof(EnemyMeleeBehavior)},
        };

        public static IEnemyBehavior Create(EnemyKind enemyKind, IEnemy model)
        {
            if (!map.ContainsKey(enemyKind))
                throw new Exception(string.Format("No Behavior type specified for {0}. Check EnemyBehaviorFactory",
                    enemyKind.ToString()));
            Type acceleratorType = map[enemyKind];
            var res = (IEnemyBehavior) Activator.CreateInstance(acceleratorType, model);
            
            return res;
        }
    }
}
