﻿using Archero.Model;
using Archero.View;
using UnityEngine;

namespace Archero.Controller.EnemyAI
{
    public class EnemyMeleeBehavior : EnemyBehavior
    {
        private Decision prevDecision = Decision.SetIdle;
        
        public EnemyMeleeBehavior(IEnemy enemy) : base(enemy)
        {           
        }
        
        protected override void MakeDecision()
        {
            prevDecision = currentDecision;
            if (prevDecision == Decision.SetIdle)
            {
                currentDecision = Random.value < 0.2f ? Decision.MoveToRandomPoint : Decision.MoveToPlayer;
                nextDecisionTime = enemy.Def.Params.MoveDistance / enemy.Def.Params.MoveSpeed;
                enemy.State = EnemyState.Moving;
            }
            else if (prevDecision == Decision.MoveToPlayer)
            {
                currentDecision = Random.value < 0.2f ? Decision.SetIdle : Decision.MoveToRandomPoint;
                nextDecisionTime = currentDecision == Decision.SetIdle ? enemy.Def.Params.StaticTime :
                    enemy.Def.Params.MoveDistance / enemy.Def.Params.MoveSpeed;
                enemy.State = currentDecision == Decision.SetIdle ? EnemyState.Idle : EnemyState.Moving;
            }
            else
            {
                currentDecision = Decision.MoveToPlayer;
                nextDecisionTime = enemy.Def.Params.MoveDistance / enemy.Def.Params.MoveSpeed;
                enemy.State = EnemyState.Moving;
            }
            
            base.MakeDecision();
        }
    }
}
