﻿using System;
using Archero.Model;

namespace Archero.Controller.EnemyAI
{   
    public enum Decision
    {
        SetIdle = 0,
        Shoot = 10,
        MoveToRandomPoint = 20,
        MoveToPlayer = 30,
    }
    
    public interface IEnemyBehavior
    {
        event Action<Decision, float> OnMakeDecision;
        void DoUpdate(float deltaTime);
    }
}
