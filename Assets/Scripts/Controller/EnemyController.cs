﻿using System;
using Archero.Controller.EnemyAI;
using Archero.Model;
using Archero.View;
using UnityEngine;

namespace Archero.Controller
{
    public class EnemyController : IDisposable
    {
        private readonly IEnemy model;
        private readonly IEnemyView view;

        public IEnemyView View => view;
        public IEnemy Model => model;

        private IEnemyBehavior enemyBehavior;

        public EnemyController(IEnemy model, IEnemyView view)
        {
            this.model = model;
            this.view = view;

            enemyBehavior = EnemyBehaviorFactory.Create(model.Def.EnemyKind, model);
            enemyBehavior.OnMakeDecision += OnBehaviorDecision;
            
            view.OnImpact += DoImpact;
            view.OnUpdate += DoUpdate;
            
            model.OnTakeDamage += DoTakeDamage;
            model.OnDestroy += DoDestroy;
            model.OnChangeState += DoChangeState;
        }

        public void Dispose()
        {            
            view.DoDestroy();
            
            enemyBehavior.OnMakeDecision -= OnBehaviorDecision;
            
            view.OnImpact -= DoImpact;
            
            model.OnTakeDamage -= DoTakeDamage;
            model.OnDestroy -= DoDestroy;
            model.OnChangeState -= DoChangeState;
        }

        private void OnBehaviorDecision(Decision decision, float time)
        {
            view.PerformDecision(decision, time);
        }

        private void DoTakeDamage(IDamageable enemy)
        {
            view.DamageEffect(model.Health, model.Def.Params.Health);
        }
        
        private void DoDestroy(IDestroyable enemy)
        {
            view.DeadEffect();
        }

        private void DoChangeState(IEnemy enemy)
        {
            
        }

        private void DoUpdate(float deltaTime)
        {
            enemyBehavior.DoUpdate(deltaTime);
        }

        private void DoImpact(Collider collider)
        {
            if (collider.CompareTag(Tags.PLAYER_BULLET))
            {
                IDamaging damaging = collider.GetComponent<IDamaging>();
                if (damaging != null)
                {
                    model.TakeDamage(damaging.Damage);
                }
            }
        }
    }
}
