﻿using System;
using Archero.Model;
using Archero.View;
using UnityEngine;

namespace Archero.Controller
{
    public class CameraController : IDisposable
    {
        private readonly ICameraModel model;
        private readonly ICameraView view;

        public CameraController(ICameraModel model, ICameraView view/*, IPlayerView playerView, Field field*/)
        {
            this.model = model;
            this.view = view;
            
            //view.Fit(field.Width + (field.Info != null ? field.Info.BorderWidth : 1f));
            //view.SetTarget(playerView.GetTransform(), model.Info.Offset);
            view.OnLateUpdate += DoMove;
        }

        public void Fit(float width)
        {
            view.Fit(width);
        }

        public void SetTarget(Transform target)
        {
            view.SetTarget(target, model.Info.Offset);
        }

        public void Dispose()
        {
            view.OnLateUpdate -= DoMove;
        }

        private void DoMove()
        {
            view.MoveByTarget(model.Info.DumpSpeed);
        }
    }
}
