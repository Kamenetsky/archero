﻿using System;
using Archero.Model;
using Archero.Utils;
using Archero.View;
using UnityEngine;

namespace Archero.Controller
{
    public class PlayerController : IDisposable
    {
        private readonly IPlayerView view = null;
        private readonly Player model = null;
        private readonly Game game;

        private float shootTimer = 0f;
        
        public PlayerController(Player model, IPlayerView view, Game game)
        {
            this.model = model;
            this.view = view;
            this.game = game;
            view.Init(model.Info.Parameters.MoveSpeed);
            view.OnUpdate += DoUpdate;
            view.OnImpact += DoImpact;
            
            model.OnTakeDamage += DoTakeDamage;
            model.OnDestroy += DoDestroy;
        }

        public void Dispose()
        {
            view.OnUpdate -= DoUpdate;
            view.OnImpact -= DoImpact;
            
            model.OnTakeDamage -= DoTakeDamage;
            model.OnDestroy -= DoDestroy;
        }

        public void Reset()
        {
            view.GetTransform().position = model.Info.InitPosition;
            view.GetTransform().rotation = Quaternion.identity;
        }

        private void DoUpdate(MoverInput input, float deltaTime)
        {
            if (shootTimer > 0)
                shootTimer -= deltaTime;
            
            if (input.move.magnitude <= float.Epsilon && shootTimer <= 0f)
            {
                view.SelectTarget(game.EnemyManager.GetTargets());
                view.Shoot(model.Damage);
                shootTimer = model.Info.Parameters.ShootSpeed;
            }
            
            view.Move(input);
        }

        private void DoImpact(Collider collider)
        {
            if (collider.CompareTag(Tags.GATE))
            {
                game.GameComplete(true);
            }
            else if (collider.CompareTag(Tags.BULLET) || collider.CompareTag(Tags.ENEMY))
            {
                IDamaging damaging = collider.GetComponent<IDamaging>();
                if (damaging != null)
                {
                    model.TakeDamage(damaging.Damage);
                }
            }
        }
        
        private void DoTakeDamage(IDamageable enemy)
        {
            view.DamageEffect(model.Health, model.MaxHealth);
        }
        
        private void DoDestroy(IDestroyable enemy)
        {
            view.DeadEffect();
            game.GameComplete(false);
        }
    }
}
