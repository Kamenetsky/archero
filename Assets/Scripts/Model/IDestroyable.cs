﻿using System;

namespace Archero.Model
{
    public interface IDestroyable
    {
        event Action<IDestroyable> OnDestroy;
     
        void Destroy();
    }
}
