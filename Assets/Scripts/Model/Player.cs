﻿using System;
using Archero.Data;

namespace Archero.Model
{
    public class Player : IDamageable, IDestroyable
    {
        public event Action<IDamageable> OnTakeDamage;
        public event Action<IDestroyable> OnDestroy;

        public PlayerInfo Info => info;
        public float Damage => info.Parameters.Damage;
        public float MaxHealth => info.Parameters.Health;
        public float Health => health;
        
        private readonly PlayerInfo info;
        private float health;

        public Player(PlayerInfo info)
        {
            this.info = info;
            health = info.Parameters.Health;
        }
        
        public virtual void TakeDamage(float damage)
        {
            health -= damage;
            OnTakeDamage?.Invoke(this);
            if (health <= 0f)
            {
                health = 0;
                Destroy();
            }
        }

        public virtual void Destroy()
        {
            OnDestroy?.Invoke(this);
        }
    }
}
