﻿using System;
using Archero.View;
using UnityEngine;

namespace Archero.Model
{
    public enum BulletType
    {
        Simple = 0,
        Ballistic = 10,
    }
    
    public enum BulletKind
    {
        Arrow = 0,
        BallSmall = 10,
        BallBig = 10,
    }
    
    [Serializable]
    public class BulletInfo
    {
        [SerializeField] private BulletKind bulletKind = BulletKind.BallSmall;
        [SerializeField] private BulletType bulletType = BulletType.Simple;
        [SerializeField] private float speed = 10f;
        [SerializeField] private BulletView prefab = null;
        
        public BulletKind BulletKind => bulletKind;
        public BulletType BulletType => bulletType;
        public float Speed => speed;
        public BulletView Prefab => prefab;
    }

    public class Bullet
    {
        public BulletInfo info;
        public float damage;
    }
}
