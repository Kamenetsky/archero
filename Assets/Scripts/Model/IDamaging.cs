﻿using System;

namespace Archero.Model
{
    public interface IDamaging
    {
        float Damage { get; }        
    }
}
