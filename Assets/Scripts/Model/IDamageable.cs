﻿using System;

namespace Archero.Model
{
    public interface IDamageable
    {
        event Action<IDamageable> OnTakeDamage;
        
        float Health { get; }  
        void TakeDamage(float damage);    
    }
}
