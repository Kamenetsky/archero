﻿using System;

namespace Archero.Model
{
    public enum EnemyType
    {
        Ground = 0,
        Flying = 10
    }

    public enum EnemyAttackType
    {
        Ranged = 0,
        Melee = 10
    }

    public enum EnemyKind
    {
        None = 0,
        Skeleton = 10,
        Blob = 20,
    }
    
    public class Enemy : IEnemy
    {
        public event Action<IDamageable> OnTakeDamage;
        public event Action<IDestroyable> OnDestroy;
        public event Action<IEnemy> OnChangeState;

        public EnemyState State
        {
            get { return state; }
            set
            {
                state = value;
                OnChangeState?.Invoke(this);
            }
        }
        public float Damage => def.Params.Damage;
        public float MaxHealth => def.Params.Health;
        public float Health => health;
        public EnemyDef Def => def;

        private EnemyState state;
        private readonly EnemyDef def;
        private float health;

        public Enemy(EnemyDef def)
        {
            this.def = def;
            health = def.Params.Health;
        }
        
        public virtual void TakeDamage(float damage)
        {
            health -= damage;
            OnTakeDamage?.Invoke(this);
            if (health <= 0f)
            {
                health = 0;
                Destroy();
            }
        }

        public virtual void Destroy()
        {
            State = EnemyState.Dead;
            OnDestroy?.Invoke(this);
        }
    }
}
