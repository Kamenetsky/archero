﻿using Archero.Data;

namespace Archero.Model
{
    public interface ICameraModel
    {
        CameraInfo Info { get; }
    }
}
