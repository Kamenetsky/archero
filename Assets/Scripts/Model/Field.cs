﻿using System;
using System.Collections.Generic;
using System.Linq;
using Archero.Data;
using Archero.Utils;
using UnityEngine;
using Random = System.Random;

namespace Archero.Model
{
    public class Cell
    {
        public CellType cellType;
        public EnemyKind enemySpawn = EnemyKind.None;

        public Cell()
        {
            cellType = CellType.Empty;
            enemySpawn = EnemyKind.None;
        }

        public Cell(Cell origin)
        {
            cellType = origin.cellType;
            enemySpawn = origin.enemySpawn;
        }
    }
    
    public class Field
    {
        private static Dictionary<CellType, float> cellProbabilities = new Dictionary<CellType, float>
        {
            {CellType.Empty, 65f},
            {CellType.Wall, 10f},
            {CellType.Water, 5f},
        };

        private const int minEnemyCount = 2;
        private const int maxEnemyCount = 5;
        private const float enemySpawnProbability = 0.05f;
        
        private Cell[,] cells;
        private FieldInfo info;

        public bool isGenerated = false;

        public FieldInfo Info => info;
        public float CellSize => info != null ? info.CellSize : 1f;
        public float Width => cells.GetLength(0) * CellSize;
        public float Height => cells.GetLength(1) * CellSize;
        public int SizeX => cells.GetLength(0);
        public int SizeY => cells.GetLength(1);

        public Field(Vector2Int size)
        {
            cells = new Cell[size.x, size.y];
        }
        
        public Field(FieldInfo info) : this(info.Size)
        {
            this.info = info;
            
            for (int i = 0; i < info.Size.x; ++i)
            {
                for (int j = 0; j < info.Size.y; ++j)
                {
                    CellInfo ci = info.Cells.Find(a => a.Coords.x == i && a.Coords.y == j);
                    SpawnPoint sp = info.SpawnPoints.Find(a => a.Coords.x == i && a.Coords.y == j);
                    Cell cell = new Cell() {cellType = ci == null ? CellType.Empty : ci.CellType, 
                        enemySpawn = sp == null ? EnemyKind.None : sp.Enemy};
                    cells[i, j] = cell;
                }
            }
        }

        public Cell this[int x, int y]
        {
            get
            {
                if (x >= cells.GetLength(0) || y >= cells.GetLength(1))
                {
                    Debug.LogError("Wrong cell coords");
                    return null;
                }
                
                return cells[x, y];
            }
            set
            {
                if (x >= cells.GetLength(0) || y >= cells.GetLength(1))
                {
                    Debug.LogError("Wrong cell coords");
                    return;
                }

                cells[x, y] = value;
            }
        }

        public static Field GenerateRandom(Vector2Int size, bool isMirrored = true)
        {
            Field result = new Field(size);
            result.isGenerated = true;

            List<float> probabilities = cellProbabilities.Values.ToList();
            DiscreteDistribution distribution = new DiscreteDistribution(probabilities, new Random());
            
            for (int i = 0; i < FieldInfo.defaultSize.x; ++i)
            {
                for (int j = 0; j < FieldInfo.defaultSize.y; ++j)
                {
                    CellType cType = CellType.Empty;
                    EnemyKind enemy = EnemyKind.None;
                    if (j < FieldInfo.defaultSize.y - 1 && (i < FieldInfo.defaultSize.x / 2 - 1 || i > FieldInfo.defaultSize.x / 2 + 1))
                    {
                        int ind = distribution.GetRandom();
                        int k = 0;
                        foreach (CellType it in Enum.GetValues(typeof(CellType)))
                        {
                            if (k == ind)
                            {
                                cType = it;
                                break;
                            }
                            ++k;
                        }
                    }

                    Cell cell = new Cell() {cellType = cType, enemySpawn = enemy};
                    
                    if (isMirrored && i > FieldInfo.defaultSize.x / 2 )
                    {
                        int mirrIndex = FieldInfo.defaultSize.x - i - 1;
                        Cell mirrCell = result[mirrIndex, j];
                        cell = new Cell(mirrCell);
                    }
                    result[i, j] = cell;
                }
            }
            
            //Spawn enemies
            int enemyCount = 0;
            int desiredEnemyCount = UnityEngine.Random.Range(minEnemyCount, maxEnemyCount);
            while (enemyCount < desiredEnemyCount)
            {
                Cell cell = result[UnityEngine.Random.Range(0, FieldInfo.defaultSize.x),
                    UnityEngine.Random.Range(FieldInfo.defaultSize.y * 2 / 3, FieldInfo.defaultSize.y)];
                
                if (cell.enemySpawn != EnemyKind.None || cell.cellType != CellType.Empty)
                    continue;

                cell.enemySpawn = UnityEngine.Random.value > 0.5f ? EnemyKind.Blob : EnemyKind.Skeleton;
                enemyCount++;
            }

            return result;
        }
    }
}
