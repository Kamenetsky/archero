﻿using Archero.Data;

namespace Archero.Model
{
    public class CameraModel : ICameraModel
    {
        private readonly CameraInfo info;
        
        public CameraInfo Info => info;

        public CameraModel(CameraInfo info)
        {
            this.info = info;
        }
    }
}
