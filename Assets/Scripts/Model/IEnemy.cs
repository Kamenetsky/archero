﻿using System;

namespace Archero.Model
{
    public enum EnemyState
    {
        Idle = 0,
        Moving = 10,
        Shooting = 20,
        Dead = 30
    }
    
    public interface IEnemy : IDamageable, IDestroyable
    {
        event Action<IEnemy> OnChangeState;
        
        EnemyState State { get; set; }
        EnemyDef Def { get; }
    }
}
