﻿using System;
using UnityEngine;

namespace Archero.Model
{
    [Serializable]
    public class CreatureParams
    {
        [SerializeField] private float health = 100f;
        [SerializeField] private float moveSpeed = 1f;
        [SerializeField] private float shootSpeed = 1f;
        [SerializeField] private float damage = 10f;

        public float Health => health;
        public float MoveSpeed => moveSpeed;
        public float ShootSpeed => shootSpeed;
        public float Damage => damage;
    }

    [Serializable]
    public class EnemyParams : CreatureParams
    {
        [SerializeField] private float moveDistance = 5f;
        [SerializeField] private float staticTime = 3f;
        
        public float MoveDistance => moveDistance;
        public float StaticTime => staticTime;
    }

    [Serializable]
    public class EnemyDef
    {
        [SerializeField] private EnemyKind enemyKind = EnemyKind.Skeleton;
        [SerializeField] private EnemyType enemyType = EnemyType.Ground;
        [SerializeField] private EnemyAttackType attackType = EnemyAttackType.Ranged;        
        [SerializeField] private EnemyParams parameters = new EnemyParams();

        public EnemyKind EnemyKind => enemyKind;
        public EnemyType EnemyType => enemyType;
        public EnemyAttackType AttackType => attackType;
        public EnemyParams Params => parameters;
    }
}
