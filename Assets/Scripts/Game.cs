﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Archero.Controller;
using Archero.Data;
using Archero.Model;
using Archero.View;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Archero
{
    public class Game : MonoBehaviour
    {
        [SerializeField] private PlayerInfo playerInfo = null;
        [SerializeField] private CameraInfo cameraInfo = null;
        [SerializeField] private List<FieldInfo> levels = new List<FieldInfo>();
        [SerializeField] private bool randomLevel = false;
        [Range(0f, 1f)]
        [SerializeField] private float randomLevelProbabilty = 0;

        private EnemyManager enemyManager;
        
        private FieldController fieldController;
        private IFieldView fieldView;

        private CameraController cameraController;
        private ICameraView cameraView;

        private PlayerController playerController;
        private IPlayerView playerView;

        private bool firstRun = true;

        public FieldController FieldController => fieldController;
        public EnemyManager EnemyManager => enemyManager;

        private const float startTime = 3;
        
        private float time;
        private bool isStarted = false;

        private void Awake()
        {
            enemyManager = GetComponentInChildren<EnemyManager>();
            enemyManager.OnAllEnemiesDie += OnLevelComplete;
            fieldView = GetComponentInChildren<IFieldView>();
            cameraView = FindObjectOfType<CameraView>();
            playerView = FindObjectOfType<PlayerView>();
            UI.OnStartGame += CreateGame;
        }

        private void OnDestroy()
        {
            enemyManager.OnAllEnemiesDie -= OnLevelComplete;
            UI.OnStartGame -= CreateGame;
            Clear();
        }

        private void Update()
        {
            if (!isStarted)
            {
                time -= Time.deltaTime;
                UI.instance.TextCountDown.text = time.ToString("0");
                if (time < 0f)
                {
                    isStarted = true;
                    UI.instance.TextCountDown.text = "";
                    StartGame();
                    time = startTime;
                }
            }
        }
        
        private void CreateGame()
        {
            Clear();

            RefreshField();
            
            Player player = new Player(playerInfo);          
            playerController = new PlayerController(player, playerView, this);
            playerController.Reset();
            
            if (cameraController != null)
                cameraController.Dispose();
            ICameraModel cameraModel = new CameraModel(cameraInfo);
            cameraController = new CameraController(cameraModel, cameraView);
            cameraController.Fit(fieldController.Model.Width + (fieldController.Model.Info != null ? fieldController.Model.Info.BorderWidth : 1f));
            cameraController.SetTarget(playerView.GetTransform());
            
            playerView.Activate(false);
            isStarted = false;
            time = startTime;
        }

        private void RefreshField()
        {
            if (fieldController != null)
            {
                fieldController.Clear();
                fieldController = null;
            }
            Field field = null;
            if ((randomLevel && randomLevelProbabilty > Random.value && !firstRun) || levels.Count == 0)
            {
                field = Field.GenerateRandom(FieldInfo.defaultSize);
            }
            else
            {
                firstRun = false;
                FieldInfo fieldInfo = levels[Random.Range(0, levels.Count())];
                field = new Field(fieldInfo);
            }
            fieldController = new FieldController(field, fieldView);
            
            enemyManager.Spawn(fieldController.GetSpawnPoints(), playerView.GetTransform());
        }

        private void StartGame()
        {
            enemyManager.Activate(true);
            playerView.Activate(true);
        }

        public void GameComplete(bool win)
        {
            if (win)
            {
                RefreshField();
                cameraController.Fit(fieldController.Model.Width + (fieldController.Model.Info != null ? fieldController.Model.Info.BorderWidth : 1f));
                playerController.Reset();
                cameraController.SetTarget(playerView.GetTransform());
                isStarted = false;
                playerView.Activate(false);
            }
            else
            {
                StartCoroutine(GameOver());
            }
        }

        private IEnumerator GameOver()
        {
            yield return new WaitForSeconds(1.5f);
            UI.instance.GameOver();
            Clear();
        }

        private void OnLevelComplete()
        {
            fieldController.ShowGate();
        }

        private void Clear()
        {
            if (fieldController != null)
            {
                fieldController.Clear();
                fieldController = null;
            }
            
            if (playerController != null)
            {
                playerController.Dispose();
                playerController = null;
            }

            if (cameraController != null)
            {
                cameraController.Dispose();
                cameraController = null;
            }
            
            enemyManager.Clear();
            playerView.Activate(false);
        }
    }
}
