﻿using System;
using System.Collections;
using System.Collections.Generic;
using Archero.Controller.EnemyAI;
using Archero.Model;
using Archero.Utils;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Random = System.Random;

namespace Archero.View
{
    public class EnemyView : MonoBehaviour, IEnemyView
    {  
        public event Action<float> OnUpdate;
        public event Action<Collider> OnImpact;
        
        [SerializeField] private List<Transform> shootPoints = new List<Transform>();
        [SerializeField] private Slider progressPrefab = null;
        
        private CreatureSimpleMover mover;
        private BulletInfo bulletInfo;
        private EnemyDef def;
        private Transform target;
        private Slider healthProgress;
        private Camera mainCam;
        private NavMeshAgent agent;
        private Rigidbody body;

        private Decision currentDecision = Decision.SetIdle;
        private Vector3 randomPoint = Vector3.zero;
        private float actTime = 0;
        
        private bool isActive = false;

        public float Damage => def.Params.Damage;

        private void Update()
        {
            if (!isActive)
                return;

            if (actTime > 0)
            {
                actTime -= Time.deltaTime;
                ProcessDecision();
            }
            
            healthProgress.transform.position = mainCam.WorldToScreenPoint(transform.position + 2.5f * Vector3.up);
            OnUpdate?.Invoke(Time.deltaTime);
        }

        public void Activate(bool value)
        {
            isActive = value;
            if (mover != null)
             mover.Stop();
        }
        
        public virtual void Init(EnemyDef def)
        {
            this.def = def;
            mainCam = Camera.main;         
            healthProgress = Instantiate(progressPrefab);
            healthProgress.transform.SetParent(UI.instance.HealthBarParent);
            if (mover == null)
                mover = gameObject.AddComponent<CreatureSimpleMover>();
            mover.Setup(def.Params.MoveSpeed);
            agent = GetComponentInChildren<NavMeshAgent>();
            if (agent != null && def.EnemyType == EnemyType.Flying)
            {
                Destroy(agent);
            }
            healthProgress.value = 1f;
        }
        
        public virtual void DamageEffect(float health, float maxHealth)
        {
            healthProgress.value = health / maxHealth;
        }

        public virtual void DeadEffect()
        {
            if (!isActive)
                return;
            
            isActive = false;
            mover.ShowDead();
            Destroy(healthProgress.gameObject);
            StartCoroutine(Hide());
        }

        public virtual void Attack()
        {
            if (bulletInfo == null || bulletInfo.Prefab == null || shootPoints.Count == 0 || target == null)
                return;
            
            transform.LookAt(target);
            mover.ShowAttack();
            var bullet = Instantiate(bulletInfo.Prefab);
            bullet.transform.position = shootPoints[0].position;
            bullet.transform.rotation = transform.rotation;
            bullet.Init(new Bullet(){damage = def.Params.Damage, info = bulletInfo});
        }

        public virtual void PerformDecision(Decision decision, float time)
        {
            currentDecision = decision;
            actTime = time;
            randomPoint = transform.position + Vector3.forward * UnityEngine.Random.Range(-5, 5) +
                          Vector3.right * UnityEngine.Random.Range(-5, 5);

            if (agent)
                agent.isStopped = true;
        }

        private void ProcessDecision()
        {
            if (currentDecision == Decision.Shoot)
            {
                Attack();
                actTime = 0f;
            }
            else if (currentDecision == Decision.MoveToPlayer)
            {
                PerformMove(target.position);
            }
            else if (currentDecision == Decision.MoveToRandomPoint)
            {
                PerformMove(randomPoint);
            }
        }

        private void PerformMove(Vector3 pos)
        {
            if (def.EnemyType == EnemyType.Flying)
            {
                transform.position = Vector3.MoveTowards(transform.position, pos,
                    def.Params.MoveSpeed * Time.deltaTime);
            }
            else
            {
                Vector3 moveVector = (pos - transform.position).normalized;
                if (agent != null)
                {
                    agent.destination = pos;
                }

                mover.Move(new MoverInput(){move = moveVector});
            }
        }

        public virtual void OnTriggerEnter(Collider other)
        {
            OnImpact?.Invoke(other);
        }

        public Transform GetTransform()
        {
            return transform;
        }

        public void SetBulletInfo(BulletInfo info)
        {
            bulletInfo = info;
        }

        public void SetTarget(Transform target)
        {
            this.target = target;
            transform.LookAt(target);
        }

        public void DoDestroy()
        {
            if (healthProgress != null)
                Destroy(healthProgress.gameObject);
            
            Destroy(gameObject);
        }

        private IEnumerator Hide()
        {
            yield return new WaitForSeconds(1);
            gameObject.SetActive(false);
        }
    }
}
