﻿using System;
using System.Collections.Generic;
using Archero.Model;
using Archero.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Archero.View
{   
    public class PlayerView : MonoBehaviour, IPlayerView
    {
        public event Action<MoverInput, float> OnUpdate;
        public event Action<Collider> OnImpact;

        [SerializeField] private List<BulletInfo> bullets = new List<BulletInfo>();
        [SerializeField] private BulletInfo currentBullet = null;
        [SerializeField] private Transform shootPoint = null;

        [SerializeField] private Slider progressPrefab = null;
        [SerializeField] private float offset = 2.5f;
        [SerializeField] private bool isJoystickPriority = true;
            
        private CreatureSimpleMover mover;
        private FloatingJoystick joystick;
        private Slider healthProgress;
        private Camera mainCam;

        private Transform currentTarget = null;
        private bool isActive = false;

        private void Start()
        {
            joystick = UI.instance.Joystick;
            mainCam = Camera.main;
            
            mover = GetComponentInChildren<CreatureSimpleMover>();
            if (mover == null)
                mover = gameObject.AddComponent<CreatureSimpleMover>();

            healthProgress = Instantiate(progressPrefab);
            healthProgress.transform.SetParent(UI.instance.HealthBarParent);
            healthProgress.transform.position = mainCam.WorldToScreenPoint(transform.position + offset * Vector3.up);
        }

        private void Update()
        {
            if (!isActive)
                return;
            
            Vector3 currentMove = Vector3.zero;
            if (isJoystickPriority)
            {
                currentMove = Vector3.forward * joystick.Vertical + Vector3.right * joystick.Horizontal;
                if (currentMove.magnitude <= float.Epsilon)
                    currentMove = Vector3.forward * Input.GetAxis("Vertical") + Vector3.right * Input.GetAxis("Horizontal");
            }
            else
            {
                currentMove = Vector3.forward * Input.GetAxis("Vertical") + Vector3.right * Input.GetAxis("Horizontal");
                if (currentMove.magnitude <= float.Epsilon)
                    currentMove = Vector3.forward * joystick.Vertical + Vector3.right * joystick.Horizontal;
            }
            
            OnUpdate?.Invoke(new MoverInput() {move = currentMove}, Time.deltaTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            OnImpact?.Invoke(other);
        }

        public void Activate(bool value)
        {
            isActive = value;
            
            if (mover != null)
                mover.Stop();
        }

        public void Init(float speed)
        {
            mover.Setup(speed);
            healthProgress.gameObject.SetActive(true);
            healthProgress.value = 1f;
        }

        public void DamageEffect(float health, float maxHealth)
        {
            healthProgress.value = health / maxHealth;
        }

        public void DeadEffect()
        {
            mover.ShowDead();
            healthProgress.gameObject.SetActive(false);
            isActive = false;
        }
        
        public void Move(MoverInput move)
        {
            if (!isActive)
                return;
            
            mover.Move(move);
            healthProgress.transform.position = mainCam.WorldToScreenPoint(transform.position + offset * Vector3.up);
        }

        public void Shoot(float dmg)
        {
            if (currentTarget == null)
                return;
            
            mover.ShowAttack();
            transform.LookAt(currentTarget);
            var bullet = Instantiate(currentBullet.Prefab);
            bullet.transform.position = shootPoint.position;
            bullet.transform.rotation = transform.rotation;
            bullet.Init(new Bullet(){damage = dmg, info = currentBullet});
        }

        public void SelectTarget(List<Transform> targets)
        {
            if (targets == null || targets.Count == 0)
            {
                currentTarget = null;
                return;
            }

            float dist = Mathf.Infinity;

            foreach (var it in targets)
            {
                float currDist = Vector3.Distance(it.position, transform.position);
                if (currDist < dist)
                {
                    dist = currDist;
                    currentTarget = it;
                }
            }
        }
        
        public Transform GetTransform()
        {
            return transform;
        }

        public void SelectWeapon(BulletKind bulletKind)
        {
            var blt = bullets.Find(x => x.BulletKind == bulletKind);
            if (blt == null)
                return;

            currentBullet = blt;
        }
    }
}
