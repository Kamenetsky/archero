﻿using System;
using UnityEngine;

namespace Archero.View
{
    public interface ICameraView
    {
        event Action OnLateUpdate;
        
        void Fit(float width);
        void MoveByTarget(float speed);
        void SetTarget(Transform target, Vector3 offset);
    }
}
