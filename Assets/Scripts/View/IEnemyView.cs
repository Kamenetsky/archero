﻿using System;
using Archero.Controller.EnemyAI;
using Archero.Model;
using UnityEngine;

namespace Archero.View
{
    public interface IEnemyView : ICreatureView, IDamaging
    {
        event Action<float> OnUpdate;
        
        void SetBulletInfo(BulletInfo info);
        void Init(EnemyDef def);
        void Attack();
        void SetTarget(Transform target);
        void PerformDecision(Decision decision, float time);
        void DoDestroy();
    }
}
