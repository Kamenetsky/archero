﻿using System;
using System.Collections.Generic;
using Archero.Model;
using Archero.Utils;
using UnityEngine;

namespace Archero.View
{
    public interface IPlayerView : ICreatureView
    {
        event Action<MoverInput, float> OnUpdate;

        void Init(float speed);
        void SelectTarget(List<Transform> targets);
        void Shoot(float dmg);
        void Move(MoverInput input);
        void SelectWeapon(BulletKind bulletKind);
    }
}
