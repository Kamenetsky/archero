﻿using Archero.Data;
using UnityEngine;

namespace Archero.View
{
    public interface IFieldView
    {
        void Clear();
        void ShowGate(float height);
        void CreateBorders(float width, float height, float borderWidth);
        GameObject CreateCell(Vector3 position, GameObject prefab);
        GameObject CreateCell(Vector3 position, CellType prefab, bool isOdd);
    }
}
