﻿using System;
using Archero.Utils;
using UnityEngine;

namespace Archero.View
{
    public interface ICreatureView
    {
        event Action<Collider> OnImpact;

        void Activate(bool value);
        void DamageEffect(float health, float maxHealth);
        void DeadEffect();
        Transform GetTransform();
    }
}
