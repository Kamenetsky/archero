﻿using System;
using System.Collections;
using System.Collections.Generic;
using Archero.Utils;
using UnityEngine;

namespace Archero.View
{
    [RequireComponent(typeof(Camera))]
    public class CameraView : MonoBehaviour, ICameraView
    {
        public event Action OnLateUpdate;
        
        private CameraFitter fitter;       
        private Transform target;
        private Vector3 offset;

        private void Awake()
        {
            fitter = GetComponent<CameraFitter>();
        }

        private void LateUpdate()
        {
            OnLateUpdate?.Invoke();
        }
        
        public void Fit(float width)
        {
            fitter.Fit(width);
        }

        public void MoveByTarget(float speed)
        {
            if (target == null)
                return;

            Vector3 desiredPos = new Vector3(0f, 0f, target.position.z);
            transform.position = Vector3.MoveTowards(transform.position, desiredPos + offset, speed * Time.deltaTime);
        }

        public void SetTarget(Transform target, Vector3 offset)
        {
            this.target = target;
            this.offset = offset;
            Vector3 desiredPos = new Vector3(0f, 0f, target.position.z);
            transform.position = desiredPos + offset;
            transform.LookAt(target);
        }
    }
}
