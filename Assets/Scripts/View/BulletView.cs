﻿using System;
using Archero.Model;
using UnityEngine;

namespace Archero.View
{
    public class BulletView : MonoBehaviour, IDamaging, IDestroyable
    {        
        public event Action<IDestroyable> OnDestroy;

        [SerializeField] private float autoDestroyTime = 5f;

        public float Damage => model.damage;

        private Bullet model;

        public virtual void Destroy()
        {
            OnDestroy?.Invoke(this);
        }

        public void Init(Bullet model)
        {
            this.model = model;
            Destroy(gameObject, autoDestroyTime);
        }
        
        protected virtual void Update()
        {
            transform.position += transform.forward * Time.deltaTime * model.info.Speed;
        }

        private void OnTriggerEnter(Collider other)
        {
            Destroy();
            Destroy(gameObject);
        }
    }
}
