﻿using System.Collections;
using System.Collections.Generic;
using Archero.Data;
using UnityEngine;

namespace Archero.View
{
    public class FieldView : MonoBehaviour, IFieldView
    {
        [SerializeField] private List<CellTypeInfo> defaultCellTypeInfo = new List<CellTypeInfo>();
        [SerializeField] private GameObject borderPrefab = null;
        [SerializeField] private GameObject gatePrefab = null;
        
        private List<GameObject> cells = new List<GameObject>();
        private List<GameObject> borders = new List<GameObject>();
        private GameObject gate = null;
        
        public void Clear()
        {
            for (int i = 0; i < cells.Count; ++i)
            {
                Destroy(cells[i]);
            }           
            cells.Clear();
            
            for (int i = 0; i < borders.Count; ++i)
            {
                Destroy(borders[i]);
            }
            borders.Clear();

            if (gate != null)
                Destroy(gate);
            gate = null;
        }

        public void ShowGate(float height)
        {
            if (gate != null)
                Destroy(gate);
            gate = Instantiate(gatePrefab);
            gate.transform.SetParent(transform);
            gate.transform.position = new Vector3(0f, 0f, (height + 1f) * 0.5f);
        }

        public void CreateBorders(float width, float height, float borderWidth)
        {
            var go = Instantiate(borderPrefab);
            go.transform.SetParent(transform);
            go.transform.position = new Vector3(0f, 0f, height * 0.5f + borderWidth);
            go.transform.localScale = new Vector3(width + 2f, 1f, 1f);
            borders.Add(go);
            
            go = Instantiate(borderPrefab);
            go.transform.SetParent(transform);
            go.transform.position = new Vector3(0f, 0f, -height * 0.5f - borderWidth);
            go.transform.localScale = new Vector3(width + 2f, 1f, 1f);
            go.GetComponentInChildren<MeshRenderer>().enabled = false;
            borders.Add(go);
            
            go = Instantiate(borderPrefab);
            go.transform.SetParent(transform);
            go.transform.position = new Vector3(-width * 0.5f - borderWidth, 0f, 0f);
            go.transform.localScale = new Vector3(1f, 1f, height);
            borders.Add(go);
            
            go = Instantiate(borderPrefab);
            go.transform.SetParent(transform);
            go.transform.position = new Vector3(width * 0.5f + borderWidth, 0f, 0f);
            go.transform.localScale = new Vector3(1f, 1f, height);
            borders.Add(go);
        }

        public GameObject CreateCell(Vector3 position, GameObject prefab)
        {
            var go = Instantiate(prefab);
            go.transform.SetParent(transform);
            go.transform.position = position;
            cells.Add(go);
            return go;
        }
        
        public GameObject CreateCell(Vector3 position, CellType cellType, bool isOdd)
        {
            var info = defaultCellTypeInfo.Find(x => x.CellType == cellType);
            var prefabs = info.Prefabs;
            var prefab = cellType == CellType.Empty ? (isOdd ? prefabs[0] : prefabs[1]) : prefabs[0];
            return CreateCell(position, prefab);
        }
    }
}
